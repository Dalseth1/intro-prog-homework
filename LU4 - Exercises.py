#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 19:46:23 2018

@author: martindalseth
"""

       ## notater ##
# dictionaries 
# DICT[KEY] --> Value
# LIST[0] --> 0th element of the list
# {key1: value1, key2: value2,... .... }
# phone_book = {'martin' :[ '93416698', 'martin.dalseth@live.no']
#, 'bob' : '1234567'}
# print(phone_book['martin']) - turns into the value stored
       ##


# Exercise 0



my_tuple = ('a', 'b', 'c')

print(my_tuple[0])
print(my_tuple[1])
print(my_tuple[2])

# Exercise 1

my_tup = (1,2,3)
my_tup[0] = 5

tuple object does not support item assignment
cant be done, because you cant change a tuple

# Exercise 2

January1_tuple_APPL = (1,2,3,4,5)
January1_tuple_APPL[0:5]

# Exercise 3
# As we can't change a tuple, nothing will happen, except an error

# Exercise 4

APPL_list_price = [2,4,3,1,4]
APPL_list_price.append(7)
print(APPL_list_price)


# Exercise 5

# They are very simillar, the difference is that tuples can't be changed
# and list can, also the brackets used is different

# Exercise 6
# I guess they are considered equal when numbers are sorted and also 
# the same value in the list and tuple

# Exercise 7

tup = (1,2,3)
list(tup)
print(list(tup))

# Exercise 8

# keys() - shows all the keys in the dictionary 
# values() - shows all the values in the dictionary 
# items() - shows all the keys and values in the dictionary 

# Exercise 9 

empty = {'' : ''}
empty_d = {'hello' : 'world', 'hello2' : 'martin'}
print(type(empty_d), empty_d)

empty_d.keys()

# Exercise 10

stock_prices = { 'APPL' : 100, 'GOOG' : 99,} 
stock_of_interest = 'APPL' 
print(stock_prices[stock_of_interest])
# it gives us the stock price of appl, which is what we want 

# Exercise 11

# By using the dict constructor and zip function one is able to make a d

first_example = {'1' : 3, '2' : 4}
first_example.keys

keys1 = (1,2)
values2 = (3,4)

d_example = dict(zip(keys1, values2))
print(d_example)


# Exercise 12

stocks = {'AAPL' : 200, 'GOOG' : 130}

# use one of the two: 

del stocks['AAPL']

stocks.pop('AAPL') 

print(stocks)

#1: del name_list[key] 
#2: name_list.pop(key)


# Exercise 13

def add_homie(name_list):
    name_list.append('anton')
    return name_list

gang = ['martin', 'sturle', 'sindre', 'magnus']

print(add_homie(gang))


# Exercise 14
# A
GOOG = 75
APPL = 125
KPMG = 55
BCG = 40
MSFT = 200
OSYS = 25
PHZR = 60

def stocks_average(a,b,c,d,e,f,g):
    av_st = (a+b+c+d+e+f+g)/7
    return av_st

print(stocks_average(GOOG, APPL, KPMG, BCG, MSFT, OSYS, PHZR))

# B

list_average_stocks = [75,125,55,40,200,25,60]

def list_average(a):
    average = sum(a)/7
    return average 

print(list_average(list_average_stocks))

# B 

stock_map = {'GOOG' : [75], 'APPL' : [125], 'KPMG' : [55],
            'BCG' : [40], 'MSFT' : [200], 'OSYS' : [25],
            'PHZR' : [60]}




















