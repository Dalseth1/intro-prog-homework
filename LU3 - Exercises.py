#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 18:02:44 2018

@author: martindalseth
"""
# Exercise 1

SCOPE: 
If a variable available is within the scope of execution, it can be used
Local Scope: A function has its own scope
and the functions scope is it's local scope
    
Global Scope:the scope of everything thats not
in a function in a file
    
    

  
# Exercise 2


row 3 = global scope
row 6-7 = local
row 9 = global scope
row 13-14 = local



"Exercise 3

When you print ('hello', first_var), without 
defining or assigning the value to first_var,
we'll get a name error, because firs_var is not 
defined. 
if we had changed the line it would work. 

# Exercise 4

t = 5

def entotre(n):
    machine = t * 2
    return machine

print(entotre)
print(t)


# Exercise 5

power = 10
print(power)

def generate_power(number):
    _power_ = 2
   ## here the local variable in the function is assigned but never used 
    def nth_power():
        return number ** power
    
powered = nth_power() 
 # nth_power()  is not defined, only in the local - cant bring it global
return powered ## we can't return outside a function 

print(_power_)
 # again a name that is only defined local 
 a = generate_power(2)
  # ikke helt sikker her, men er samme greiene. 
  en kan ikke ta local ut til golbal
print(a)

# Exercise 6

power = 10
print(power)

def generate_power(number):
     power = 2
     def nth_power():
        return number ** power
# as we don't return power=2 we don't get anything from the function
# difference here is that 
powered  = nth_power()
return powered

print(power)
a = generate_power(2)
print(a) 


# Exercise 7 

# what do i expect: the first one is gobal and the second is local 

_what = 'coins'
_for = 'chocolates'

def transaction(_what, _for):
    _what = 'oranges'
    _for = 'bananas'
    print(_what, _for)

transaction(_what, _for)
print(_what, _for)

# they are different because of the scope, we call the function
# and get the local output, when we call the two global variables 
# we get a different output

# Exercise 8

# Scope is the reach of a variable
# Local scope is the reach (scope) inside a function
# Global scope is the reach (scope) inside the whole file, exept the function

# Exercise 9

def names(first_name = 'martin', last_name = 'dalseth'):
    print(first_name, last_name)
    
names()
names(first_name = 'martin', last_name = 'dalseth')
names('martin', 'dalseth')
  
# Exercise 10

first_name = 'ricardo'
last_name = 'pereira'

def names(first_name = 'martin', last_name = 'dalseth'):
    print(first_name, last_name)

names()
names(first_name = first_name, last_name = last_name)
names(first_name = 'jose', last_name = 'maria')

  # the first: default arguments 
  # in the second we set arguments equal to the global variables we assigned 
  # third: set arguments equal to names
  
# Exercise 11

def message(name = '', message = 'good morning'):
    print(message, name)
    
message()
message(name = 'John Snow', message = 'You know nothing')


def print_trade(ticker_symbol = 'AAPL', num_stocks = 5):
    print('martin wants to trade', num_stocks, 'of', ticker_symbol)
    
print_trade(ticker_symbol = 'GOOG', num_stocks = 3500)



    


  


     
     
     
     
     
     
     
     
     
     
     
     
     